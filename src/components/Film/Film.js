import React, { Component } from 'react';

import './Film.css'

class Film extends Component {
    render() {
        return (
            <div className="Film">
                <div className="Film-img">
                    <img src={this.props.poster} alt={this.props.name} />
                </div>
                <div className="Film-txt">
                    <h4>{this.props.name}</h4>
                    <span>{this.props.year}</span>
                </div>
            </div>
        )
    }
}

export default Film;
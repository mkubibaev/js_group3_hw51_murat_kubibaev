import React, { Component } from 'react';

import "./Content.css";

class Content extends Component {
    render() {
        return (
            <div className="content">
                <h2>Films</h2>
                {this.props.children}
            </div>
        );
    }
}

export default Content;
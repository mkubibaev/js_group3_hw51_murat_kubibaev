import React, { Component } from 'react';

import "./Header.css";

class Header extends Component {
    render() {
        return (
            <header className="Header">
                <div className="container">
                    <h1>Site title</h1>
                </div>
            </header>
        );
    }
}

export default Header;
import React, { Component } from 'react';

import "./Sidebar.css";

class Sidebar extends Component {
    render() {
        return (
            <aside className="sidebar">
                {this.props.children}
            </aside>
        );
    }
}

export default Sidebar;
import React, { Component } from 'react';

import './Menu.css'

class Menu extends Component {
    render() {
        return (
            <div className="Menu">
                <p>Menu</p>
                <ul>
                    <li>menu-item</li>
                    <li>menu-item</li>
                    <li>menu-item</li>
                    <li>menu-item</li>
                    <li>menu-item</li>
                </ul>
            </div>
        )
    }
}

export default Menu;
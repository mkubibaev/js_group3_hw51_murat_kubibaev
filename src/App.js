import React, { Component } from 'react';
import Header from './components/layout/Header/Header';
import Sidebar from './components/layout/Sidebar/Sidebar';
import Content from './components/layout/Content/Content';
import Menu from './components/Menu/Menu';
import Film from './components/Film/Film';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Header />
                <div className="container">
                    <Sidebar>
                        <Menu />
                    </Sidebar>
                    <Content>
                        <Film name="Inception"
                              year={2010}
                              poster="https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SY1000_CR0,0,675,1000_AL_.jpg" />
                        <Film name="The Lord of rings"
                              year={2001}
                              poster="https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_SY999_CR0,0,673,999_AL_.jpg" />
                        <Film name="Casino Royale"
                              year={2006}
                              poster="https://m.media-amazon.com/images/M/MV5BMDI5ZWJhOWItYTlhOC00YWNhLTlkNzctNDU5YTI1M2E1MWZhXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SY1000_CR0,0,672,1000_AL_.jpg" />

                    </Content>
                </div>
            </div>
        );
    }
}

export default App;
